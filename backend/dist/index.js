"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const app = express_1.default();
app.set("port", process.env.PORT || 3000);
var http = require("http").Server(app);
let io = require("socket.io")(http);
let messages = new Array();
let users = new Array();
// simple '/' endpoint sending a Hello World
// response
app.get("/", (req, res) => {
    res.send("hello world");
});
io.on("connection", function (socket) {
    var address = socket.handshake.address;
    console.log(`a user connected ${address}`);
    socket.emit("messages", messages);
    socket.emit("userList", users);
    users.push({ id: socket.id.substring(0, 6), name: "" });
    socket.on("message", function (message) {
        console.log(message);
    });
    // socket.emit("userList", io.sockets.)
    socket.on("sendMessage", function (message) {
        console.log("Incoming newMessage:");
        console.log(message);
        messages.push(message);
        io.sockets.emit("messages", messages);
    });
    socket.on("disconnect", (reason) => {
        users = users.filter(function (obj) {
            return obj.id !== socket.id.substring(0, 6);
        });
        console.log("User left the Server..");
        io.sockets.emit("userList", messages);
    });
});
// start our simple server up on localhost:3000
const server = http.listen(3000, function () {
    console.log("listening on *:3000");
});
//# sourceMappingURL=index.js.map