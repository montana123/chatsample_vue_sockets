import * as socketio from "socket.io";
import express from "express";

import { Socket } from "socket.io";

const app = express();
app.set("port", process.env.PORT || 3000);

var http = require("http").Server(app);
let io = require("socket.io")(http);

export interface UserMessage {
  user: string;
  date: string;
  msg: string;
}

let messages: Array<UserMessage> = new Array<UserMessage>();

export interface User {
  id: string;
  name: string;
}

let users: Array<User> = new Array<User>();

// simple '/' endpoint sending a Hello World
// response
app.get("/", (req: any, res: any) => {
  res.send("hello world");
});

io.on("connection", function (socket: Socket) {
  var address = socket.handshake.address;
  console.log(`a user connected ${address}`);
  socket.emit("messages", messages);
  socket.emit("userList", users);
  users.push({ id: socket.id.substring(0, 6), name: "" });

  socket.on("message", function (message: any) {
    console.log(message);
  });

  // socket.emit("userList", io.sockets.)

  socket.on("sendMessage", function (message: UserMessage) {
    console.log("Incoming newMessage:");
    console.log(message);
    messages.push(message);
    io.sockets.emit("messages", messages);
  });

  socket.on("disconnect", (reason) => {
    users = users.filter(function (obj) {
      return obj.id !== socket.id.substring(0, 6);
    });
    console.log("User left the Server..");
    io.sockets.emit("userList", messages);
  });
});

// start our simple server up on localhost:3000
const server = http.listen(3000, function () {
  console.log("listening on *:3000");
});
